import React, {Component} from 'react';
import 'antd/dist/antd.css';
import '../index.css';
import {Form, Input, DatePicker, Button, Upload, Modal} from 'antd';
import {Icon } from 'antd';

const {RangePicker } = DatePicker;
const formItemLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 },
};

const formItemLayout2 = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16},
};

const formItemLayoutForUploader = {
    labelCol: { span: 8 },
    wrapperCol: { span: 14 },
};

class AddCourseModal extends Component{
    constructor(props)
    {
        super(props);

    }

    handleOkAdd(){

    }

    render() {
        const { getFieldDecorator} = this.props.form;
        const rangeConfig = {
            rules: [{ type: 'array', required: true, message: 'Please select time!' }],
        };

        return(
                <Modal
                    title= "Add Course Manterial"
                    visible={this.props.visibility}
                    onOk={this.handleOkAdd}
                    onCancel={this.props.cancelFunc}
                >
                    <Form.Item {...formItemLayout} label="name">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input material name',
                                },
                            ],
                        })(<Input placeholder= "name" />)}
                    </Form.Item>
                    <Form.Item {...formItemLayout2} label=" Description">
                        {getFieldDecorator('description_material', {
                            rules: [
                                {
                                    required: false,
                                    // message: 'Please input module code',
                                },
                            ],
                        })(<Input placeholder="description" />)}
                    </Form.Item>

                    <Form.Item {...formItemLayout2} label="RangePicker">
                        {getFieldDecorator('range-picker', rangeConfig)(<RangePicker />)}
                    </Form.Item>

                    <Form.Item {...formItemLayoutForUploader} label="Upload" extra="Select from your PC">
                        {getFieldDecorator('upload', {
                            valuePropName: 'fileList',
                            getValueFromEvent: this.normFile,
                        })(
                            <Upload name="logo" action="/upload.do" listType="picture">
                                <Button>
                                    <Icon type="upload" /> Click to upload
                                </Button>
                            </Upload>,
                        )}
                    </Form.Item>

                </Modal>
        )
    }
}
const AddCourseModalComponent = Form.create({ name: 'cont_element' })(AddCourseModal);
export default AddCourseModalComponent