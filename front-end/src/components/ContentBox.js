import React, {Component, useState, useCallback} from 'react';
import 'antd/dist/antd.css';
import '../index.css';
import {Row, Button, Icon, Form, Input, Modal} from 'antd';
import ContentElementComp from "./ContentElement";
import axios from 'axios'


const formItemLayout2 = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
};

class ContentBox extends Component {

    rowAddedModules = {}

    constructor(props) {
        super(props);
        this.state = {
            addedModules: this.generateModuleList(),
            visible_new_course_modal: false,
        };

        this.deleteItem = this.deleteItem.bind(this);
        this.getAllModules = this.getAllModules.bind(this);

    }

    componentDidMount() {
        this.getAllModules()
    }

    getAllModules() {
        this.rowAddedModules = {}
        this.setState({
            addedModules: []
        })

        axios.get("http://localhost:8080/instructor/modules/")
            .then(res => {
                for (var i = 0; i < res.data.length; i++) {
                    this.rowAddedModules[i] = res.data[i];
                    console.log(res.data[i]);
                    this.setState({
                            addedModules: this.generateModuleList()
                        }
                    );
                }

            });
    }


    generateModuleList() {
        var modules = [];
        var arr = {};
        var key;
        for ((key) in this.rowAddedModules) {
            if (Object.keys(arr).length < 3) {
                arr[key] = this.rowAddedModules[key]
            } else {
                modules.push(arr);
                arr = {};
                arr[key] = this.rowAddedModules[key]
            }
        }
        modules.push(arr);
        return modules;
    }


    showNewCourseModal = () => {
        // setVisible(true);
        this.setState({
            visible_new_course_modal: true,
            visible: true
        });

    };
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }
            const data = {
                module_code: fieldsValue['module_code'],
                module_name: fieldsValue['module_name'],
                description: fieldsValue['description']
            };

            const headers = {
                'Content-Type': 'application/json'
            }
            axios.post("http://localhost:8080/instructor/modules/", data, headers).then(res => {
                console.log(res);
                console.log(res.data);
                this.setState({
                    visible_new_course_modal: false,
                });
                this.getAllModules();

            });
        })
    };

    handleCancelNewCourse = e => {
        console.log(e.value);
        this.setState({
            visible_new_course_modal: false,
        });
    };


    deleteItem(course_id) {
        var module_code = this.rowAddedModules[course_id]['module_code'];
        console.log(module_code);
        axios.delete("http://localhost:8080/instructor/modules/" + module_code)
            .then(res => {
                console.log(res);
                // delete this.rowAddedModules[course_id];
                this.getAllModules();
                // this.setState({
                //         addedModules: this.generateModuleList(),
                // })

            });
    }

    render() {

        if (this.state.addedModules == null) {
            return null;
        } else {
            const {getFieldDecorator} = this.props.form;
            return (
                <div style={{background: '#ECECEC', padding: '30px'}}>
                    <Button style={{background: '#161216', width: 100, height: 50, color: '#ECECEC'}}
                            onClick={this.showNewCourseModal}>ADD</Button>
                    {this.state.addedModules.map(item =>
                        <Row gutter={16}>
                            {
                                Object.keys(item).map(item1 => <ContentElementComp delete_item={this.deleteItem}
                                                                                   course_id={item1}
                                                                                   name={item[item1].module_name}
                                                                                   module_code={item[item1].module_code}
                                                                                   description={item[item1].description}
                                                                                    getAllModulesFunc = {this.getAllModules}/>)}

                        </Row>)}
                    <Modal
                        title="Add New Course"
                        visible={this.state.visible_new_course_modal}
                        onOk={this.handleSubmit}
                        onCancel={this.handleCancelNewCourse}


                    >


                        <Form.Item {...formItemLayout2} label="Module Code">
                            {getFieldDecorator('module_code', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input module code',
                                    },
                                ],
                            })(<Input/>)}
                        </Form.Item>

                        <Form.Item {...formItemLayout2} label="Module name">
                            {getFieldDecorator('module_name', {
                                rules: [
                                    {
                                        required: true,
                                        message: 'Please input module name',
                                    },
                                ],
                            })(<Input name="mod_name"/>)}
                        </Form.Item>

                        <Form.Item {...formItemLayout2} label=" Description">
                            {getFieldDecorator('description', {
                                rules: [
                                    {
                                        required: false,
                                        // message: 'Please input module code',
                                    },
                                ],
                            })(<Input/>)}
                        </Form.Item>
                    </Modal>

                </div>
            )
        }
    }

}

const ContentBoxModules = Form.create({name: 'cont_box'})(ContentBox);
export default ContentBoxModules
