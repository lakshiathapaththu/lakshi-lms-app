import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import '../index.css';
import {Card, Col, Avatar, Modal, Form, Input} from 'antd';
import axios from "axios";

const formItemLayout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 },
};

const formItemLayout2 = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16},
};

class EditModal extends Component{
    constructor(props)
    {
        super(props);
        this.state = {
            visible_edit_modal: this.props.visible_edit_modal,
        }
    }


    handleOkEdit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                console.log("Error Happen");
                return;
            }
            var modName;
            var description;

            if(fieldsValue['modulename'] == undefined){
                modName = this.props.course_name;
            }
            else{
                modName = fieldsValue['modulename']
            }

            if(fieldsValue['description'] == undefined){
                description = this.props.description;
            }
            else{
                description = fieldsValue['description']
            }
            const data = {
                module_code: this.props.module_code,
                module_name: modName,
                description: description
            };

            const headers = {'Content-Type': 'application/json'};

            axios.put("http://localhost:8080/instructor/modules/",  data, headers).
                then(res => {
                    console.log(res);
                    console.log(res.data);
                    // this.setState({
                    //     visible_new_course_modal: false,
                    // });

                    this.props.cancelFunc();
                    this.props.getAllModulesFunc();
                });

            console.log(data)
        })
    };

    render() {
        const { getFieldDecorator, getFieldValue } = this.props.form;
        const rangeConfig = {
            rules: [{ type: 'array', required: true, message: 'Please select time!' }],
        };

        return(
                <Modal
                    title= {this.props.course_name}
                    visible={this.props.visibility}
                    onOk={this.handleOkEdit}
                    onCancel={this.props.cancelFunc}
                >
                    <Form.Item {...formItemLayout} label="Module Code">
                        {getFieldDecorator('modulecode', {
                            rules: [
                                {
                                    required: false,
                                    // message: 'Please input module code',
                                },
                            ],
                        })(<Input value={this.state.module_code} readOnly={"readonly"} placeholder={this.props.module_code}/>)}
                    </Form.Item>

                    <Form.Item {...formItemLayout} label="Module name">
                        {getFieldDecorator('modulename', {
                            rules: [
                                {
                                    required: false,
                                    // message: 'Please input module name',
                                },
                            ],
                        })(<Input placeholder= {this.props.course_name} value={this.props.module_code} />)}
                    </Form.Item>



                    <Form.Item {...formItemLayout2} label=" Description">
                        {getFieldDecorator('description', {
                            rules: [
                                {
                                    required: false,
                                    // message: 'Please input module code',
                                },
                            ],
                        })(<Input placeholder={this.props.description} value={this.props.description}/>)}
                    </Form.Item>
                </Modal>
        )
    }
}
const EditModalComponent = Form.create({ name: 'cont_element' })(EditModal);
export default EditModalComponent