import React, {Component} from 'react';
import 'antd/dist/antd.css';
import '../index.css';
import {Modal, Form} from 'antd';

class ViewModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    handleOkView = e => {
        console.log(e);
        this.setState({
            visible_view_modal: false,
        });
    };


    render() {
        return (
            <Modal title={this.props.course_name}
                   visible={this.props.visibility}
                   onOk={this.handleOkView}
                   onCancel={this.props.cancelFunc}>


                <h3>Module Code : {this.props.module_code}</h3>
                <h3>Course Name : {this.props.course_name}</h3>
                <h3>Course description : {this.props.description}</h3>
                <h3>Course Name : course material</h3>
            </Modal>)

    }
}
const ViewModalComponent = Form.create({ name: 'cont_element' })(ViewModal);
export default ViewModalComponent