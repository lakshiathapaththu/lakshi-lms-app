package Instructor.CourseMaterial;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

//@ComponentScan({"Instructor.CourseMaterial","Instructor.CourseModule"})
@ComponentScan({"Instructor"})
@SpringBootApplication
public class LMSInstructorNew {
    public static void main(String[] args) {
            SpringApplication.run(LMSInstructorNew.class, args);

    }
}
