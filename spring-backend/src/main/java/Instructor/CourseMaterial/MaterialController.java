package Instructor.CourseMaterial;

//import Instructor.CourseModule.Module;
import Instructor.CourseModule.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MaterialController {

    @Autowired
    private MaterialService material_service;

    @RequestMapping("instructor/modules/{module_code}/material")
    public List<Material> getAllModules(@PathVariable String module_code){
        System.out.println("come for");
        return material_service.getAllMaterials(module_code);

    }

    @RequestMapping("instructor/modules/{module_code}/material/{material_id}")
    public Material getModule(@PathVariable int material_id){
        return material_service.getMaterial(material_id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/instructor/modules/{module_code}/material")
    public void addModule(@RequestBody Material material, @PathVariable String module_code){
        material.setModule(new Module(module_code, " hi hi", "hi hi"));
        material_service.addMaterial(material);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/instructor/modules/{module_code}/material/{material_id}")
    public void updateModule(@RequestBody Material material, @PathVariable String module_code){
        material.setModule(new Module(module_code, " hi hi", "hi hi"));
        material_service.updateModule(material);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/instructor/modules/{module_code}/material/{material_id}")
    public void deleteModule(@PathVariable int material_id){
        material_service.deleteModule(material_id);
    }


}
