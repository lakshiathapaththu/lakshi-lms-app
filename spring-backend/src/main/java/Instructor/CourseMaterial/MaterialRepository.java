package Instructor.CourseMaterial;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//@Repository
public interface MaterialRepository extends CrudRepository<Material, Integer> {

//    public  List<Material> findByModuleId(String moduleId);
}
