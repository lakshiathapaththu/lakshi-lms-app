package Instructor.CourseMaterial;
//import Instructor.CourseModule.Module;
import Instructor.CourseModule.Module;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;

@Entity
public class Material {


//    primary key of module table
    @Id
    private int material_id;
    private String name;
    private String description;
    private String duration;

    @ManyToOne
    private Module module1;

    public Material(){

    }


    public Material(int material_id, String name, String description, String duration, String module_id) {
        super();
        this.material_id = material_id;
        this.name = name;
        this.description = description;
        this.duration = duration;
        this.module1 = new Module(module_id,"abc","abc");
    }

    public Module getModule() {
        return module1;
    }

    public void setModule(Module module) {
        this.module1 = module;
    }

    public int getMaterial_id() {
        return material_id;
    }

    public void setMaterial_id(int material_id) {
        this.material_id = material_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
