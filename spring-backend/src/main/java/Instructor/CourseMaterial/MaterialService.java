package Instructor.CourseMaterial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MaterialService {

    @Autowired
    private MaterialRepository materialRepository;

    public List<Material> getAllMaterials(String module_code){
       List<Material> allMaterials = new ArrayList<>();
       materialRepository.findAll()
               .forEach(allMaterials::add);
       return allMaterials;
    }

    public Material getMaterial(int material_id){
//        return modules.stream().filter(m->m.getModule_id() == id).findFirst().get();
          return materialRepository.findOne(material_id);
    }

    public void addMaterial(Material material) {
        materialRepository.save(material);
    }

    public void updateModule(Material material) {
       materialRepository.save(material);
       //update if there is a course already exists with the same key
    }

    public void deleteModule(int material_id) {
        materialRepository.delete(material_id);
    }
}
