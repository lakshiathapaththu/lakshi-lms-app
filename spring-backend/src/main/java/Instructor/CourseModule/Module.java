package Instructor.CourseModule;


import javax.persistence.*;

@Entity
public class Module {


//    primary key of module table
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String moduleCode;
    private String module_name;
    private String description;

    public Module(){

    }

    public Module(String module_code, String module_name, String description) {
        this.moduleCode = module_code;
        this.module_name = module_name;
        this.description = description;
    }

    public String getModule_code() {
        return moduleCode;
    }

    public void setModule_code(String module_code) {
        this.moduleCode = module_code;
    }

    public String getModule_name() {
        return module_name;
    }

    public void setModule_name(String module_name) {
        this.module_name = module_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
