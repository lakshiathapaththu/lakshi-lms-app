package Instructor.CourseModule;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ModuleRepository extends CrudRepository<Module, Long> {
    Long deleteByModuleCode(String module_code);


    @Modifying
    @Query("update Module m set m.module_name = :module_name, m.description = :description where m.moduleCode = :moduleCode")
    void updateModuleSetModuleforModuleCode(@Param("module_name") String module_name,
                                   @Param("description") String description,
                                   @Param("moduleCode") String moduleCode);



}