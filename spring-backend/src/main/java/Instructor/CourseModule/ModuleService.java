package Instructor.CourseModule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ModuleService{

    @Autowired
    private ModuleRepository moduleReposiroty;

    public List<Module> getAllModules(){
       List<Module> allModules = new ArrayList<>();
       moduleReposiroty.findAll().forEach(allModules::add);
       return allModules;
    }

//    public Module getModule(String module_code){
////        return modules.stream().filter(m->m.getModule_id() == id).findFirst().get();
//          return moduleReposiroty.findOne(module_code);
//    }

    public void addModule(Module module) {
        moduleReposiroty.save(module);
    }

    public void updateModule(String module_name, String description, String module_code) {
//       moduleReposiroty.save(module);
       //update if there is a course already exists with the same key
        moduleReposiroty.updateModuleSetModuleforModuleCode(module_name,description,module_code);
    }

    public void deleteModule(String module_code) {
        moduleReposiroty.deleteByModuleCode(module_code);
    }
}
