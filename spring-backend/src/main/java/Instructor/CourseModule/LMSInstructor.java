package Instructor.CourseModule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@ComponentScan({"Instructor.CourseMaterial","Instructor.CourseModule"})
//@ComponentScan({"Instructor"})
@SpringBootApplication
public class LMSInstructor {
    public static void main(String[] args) {
            SpringApplication.run(LMSInstructor.class, args);

    }
}
