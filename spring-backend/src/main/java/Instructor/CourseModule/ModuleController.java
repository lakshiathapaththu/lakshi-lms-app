package Instructor.CourseModule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ModuleController {

    @Autowired
    private ModuleService module_service;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping("instructor/modules/" )
    public List<Module> getAllModules(){
        return module_service.getAllModules();
    }

//    @RequestMapping("instructor/modules/{module_code}")
//    public Module getModule(@PathVariable String module_code){
//        return module_service.getModule(module_code);
//    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method = RequestMethod.POST, value = "/instructor/modules/")
    public void addModule(@RequestBody Module module)
    {

        System.out.println(module.getModule_name());
        module_service.addModule(module);

    }

//    @CrossOrigin(origins = "http://localhost:3000")
//    @RequestMapping(method = RequestMethod.PUT, value = "/instructor/modules/{module_code}")
//    public void updateModule(@RequestBody Module module, @PathVariable String module_code){
//        module_service.updateModule(module,module_code);
//    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method = RequestMethod.DELETE, value = "/instructor/modules/{module_code}")
    public void deleteModule(@PathVariable String module_code){
        module_service.deleteModule(module_code);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method = RequestMethod.PUT, value = "/instructor/modules/")
    public void updateModule(@RequestBody Module module){
        String module_name = module.getModule_name();
        String description = module.getDescription();
        String module_code = module.getModule_code();
        module_service.updateModule(module_name,description,module_code);
    }



}
